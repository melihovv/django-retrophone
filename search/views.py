from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse

from phones.models import Phone


def search(request):
    query = request.GET['query']
    results = []

    if query:
        search_results = Phone.objects.filter(
            Q(name__icontains=query)
            | Q(desc__icontains=query)
        )

        for result in search_results:
            results.append({
                'url': reverse('phones:detail', args=[result.id]),
                'header': result.name,
            })

    return render(request, 'search/results.html', {
        'query': query,
        'results': results,
    })
