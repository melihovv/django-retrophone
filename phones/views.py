import json
from datetime import datetime, timedelta

from django.core.mail import send_mail
from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.views import generic

from phones.models import Phone
from retrophone.private_settings import EMAIL_HOST_USER
from retrophone.private_settings import EMAIL_TO


class IndexView(generic.ListView):
    template_name = 'phones/index.html'
    context_object_name = 'phones'

    def get_queryset(self):
        return Phone.objects.all()


class DetailView(generic.DetailView):
    model = Phone
    template_name = 'phones/detail.html'

    def render_to_response(self, context, **response_kwargs):
        date = datetime.now() + timedelta(days=3)
        context['date'] = date.strftime('%Y-%m-%d')
        return super().render_to_response(context, **response_kwargs)


def order(request, pk):
    if request.method != 'POST':
        raise Http404

    book = get_object_or_404(Phone, pk=pk)

    payload = json.loads(request.body.decode('utf-8'))
    context = {
        'email': payload['email'],
        'bookName': book.name,
        'amount': payload['amount'],
    }

    send_mail(
        'Заказ телефона',
        render_to_string('phones/email.txt', context),
        EMAIL_HOST_USER,
        [EMAIL_TO],
        fail_silently=False,
        html_message=render_to_string('phones/email.html', context)
    )

    return HttpResponse('Ok')
