import os

from django.db import models


def get_image_path(instance, filename):
    return os.path.join('phones', str(instance.id), filename)


class Phone(models.Model):
    name = models.CharField(max_length=255)
    desc = models.TextField()
    short_desc = models.TextField()
    photo = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    price = models.IntegerField()

    def __str__(self):
        return self.name
