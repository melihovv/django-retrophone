import json

from django.core.mail import send_mail
from django.http import Http404
from django.http import HttpResponse
from django.template.loader import render_to_string

from retrophone.private_settings import EMAIL_HOST_USER, EMAIL_TO


def send(request):
    if request.method != 'POST':
        raise Http404

    context = {
        'message': json.loads(request.body.decode('utf-8'))['message'],
    }
    send_mail(
        'Сообщение с формы обратной связи',
        render_to_string('feedback/email.txt', context),
        EMAIL_HOST_USER,
        [EMAIL_TO],
        fail_silently=False,
        html_message=render_to_string('feedback/email.html', context)
    )

    return HttpResponse('Ok')
