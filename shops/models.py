import os

from django.db import models


def get_image_path(instance, filename):
    return os.path.join('shops', str(instance.id), filename)


class Shop(models.Model):
    name = models.CharField(max_length=255)
    desc = models.TextField()
    photo = models.ImageField(upload_to=get_image_path, blank=True, null=True)

    def __str__(self):
        return self.name
