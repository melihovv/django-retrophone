from django.views import generic

from shops.models import Shop


class IndexView(generic.ListView):
    template_name = 'shops/index.html'
    context_object_name = 'shops'

    def get_queryset(self):
        return Shop.objects.all()
